import { Injectable } from '@angular/core';
import { PersonDetails } from './person-details/person-details.model';
import { PersonDetailsService } from './person-details/person-details.service';

// #Services
@Injectable({
  providedIn: 'root'
})
export class PersonCollectionService {
  private key = 'person-collection';
  // tslint:disable-next-line: variable-name
  private _collection: PersonDetails[] = [];

  public get collection(): PersonDetails[] {
    return this._collection;
  }

  constructor(private personDetailsService: PersonDetailsService) {
    this._collection = JSON.parse(localStorage.getItem(this.key));
  }

  // Checks if the person is in the collection
  public isInCollection(id: number): boolean {
    if (this._collection && this._collection.find(x => x.id === id)) {
      return true;
    }
    return false;
  }

  // Adds a person to the collection
  public addPerson(person: PersonDetails) {
    if (this.isInCollection(person.id)) {
      return;
    }

    if (!this.collection) {
      this._collection = [person];
    } else {
      this._collection.push(person);
    }
    localStorage.setItem(this.key, JSON.stringify(this._collection));
  }

  // Removes a person from the collection
  public removePerson(person: PersonDetails) {
    if (!this.isInCollection(person.id)) {
      return;
    }
    this._collection = this._collection.filter(x => x.id !== person.id);
    localStorage.setItem(this.key, JSON.stringify(this._collection));
  }

  // Adds a person to the collection by ID
  public addPersonById(id: number) {
    if (this.isInCollection(id)) {
      return;
    }

    this.personDetailsService.getPersonDetails('' + id).subscribe(resp => {
      if (!resp) {
        return;
      }
      if (this.isInCollection(id)) {
        return;
      }
      if (!this.collection) {
        this._collection = [resp];
      } else {
        this._collection.push(resp);
      }
      localStorage.setItem(this.key, JSON.stringify(this._collection));
    },
    err => console.error('Error: ', err));
  }

  // Removes a person from the collection by ID
  public removePersonById(id: number) {
    if (!this.isInCollection(id)) {
      return;
    }
    this._collection = this._collection.filter(x => x.id !== id);
    localStorage.setItem(this.key, JSON.stringify(this._collection));
  }

  // Sort collection by name
  public sortByName(): void {
    if (!this._collection) {
      return;
    }

    this._collection.sort((a, b) => {
      // Uppercase so it's case-insensative
      const textA = a.name.toUpperCase();
      const textB = b.name.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
    localStorage.setItem(this.key, JSON.stringify(this._collection));
  }
}
