import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { AuthService } from './auth/auth.service';
import { AuthModule } from './auth/auth.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MovieSearchModule } from './movie-search/movie-search.module';
import { AuthGuardService } from './auth/auth-guard.service';
import { CustomHttpInterceptor } from './app.custom-http-intercepter';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MovieDetailsModule } from './movie-details/movie-details.module';
import { MovieCollectionService } from './movie-collection.service';
import { PersonSearchModule } from './person-search/person-search.module';
import { PersonDetailsModule } from './person-details/person-details.module';
import { MatIconModule } from '@angular/material/icon';
import { ManageFavoriteButtonModule } from './manage-favorite-button/manage-favorite-button.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AuthModule,
    BrowserAnimationsModule,
    MovieSearchModule,
    MatButtonModule,
    RouterModule,
    CommonModule,
    MovieDetailsModule,
    PersonSearchModule,
    PersonDetailsModule,
    MatIconModule,
    MatButtonModule,
    ManageFavoriteButtonModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: CustomHttpInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
