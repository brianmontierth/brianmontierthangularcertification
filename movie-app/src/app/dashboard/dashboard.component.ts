import { Component, OnInit } from '@angular/core';
import { MovieCollectionService } from '../movie-collection.service';
import { PersonCollectionService } from '../person-collection.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(public movieCollection: MovieCollectionService, public personCollection: PersonCollectionService) { }

  ngOnInit(): void {
  }
}
