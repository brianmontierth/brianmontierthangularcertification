import { TestBed } from '@angular/core/testing';

import { PersonSearchService } from './person-search.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { PersonSearchFiltersComponent } from './person-search-filters/person-search-filters.component';
import { PersonSearchModule } from './person-search.module';
import { PersonSearchResultsComponent } from './person-search-results/person-search-results.component';

describe('PersonSearchService', () => {
  let service: PersonSearchService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule, PersonSearchModule]
    });
    service = TestBed.inject(PersonSearchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
