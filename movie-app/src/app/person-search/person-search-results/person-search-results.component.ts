import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { PersonSearchResults, Result } from './person-search-results.model';
import { PersonSearchService } from '../person-search.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { PersonCollectionService } from 'src/app/person-collection.service';

@Component({
  selector: 'app-person-search-results',
  templateUrl: './person-search-results.component.html',
  styleUrls: ['./person-search-results.component.scss']
})
export class PersonSearchResultsComponent implements OnInit, OnDestroy {

  private _query: string;

  public noResults = false;
  public results: Result[];
  public totalResults: number;

  @ViewChild('paginator', {static: false}) paginator: MatPaginator;

  private queryParamsSubscription: Subscription;
  private resultsSubscription: Subscription;

  displayedColumns: string[] = ['Name', 'Known For', 'Favorite'];

  constructor(private searchSearvice: PersonSearchService,
              private _route: ActivatedRoute,
              public personCollection: PersonCollectionService) { }

  ngOnInit(): void {
    this.queryParamsSubscription = this._route.queryParams.subscribe(params => this.paramsChanged(params));
  }

  ngOnDestroy(): void {
    if (this.queryParamsSubscription) {
      this.queryParamsSubscription.unsubscribe();
    }

    if (this.resultsSubscription) {
      this.resultsSubscription.unsubscribe();
    }
  }

  private paramsChanged(params: Params) {
    if (this.paginator) {
      this.paginator.pageIndex = 0;
    }

    this._query = params['query'];

    if (!this._query) {
      this.results = [];
      this.totalResults = 0;
      this.noResults = false;
      return;
    }

    // #Observables
    this.resultsSubscription = this.searchSearvice.searchPerson(this._query).subscribe(data => {
    this.results = data.results;
    this.totalResults = data.total_results;

    this.noResults = this.totalResults < 1;
   },
   err => console.error('Error: ', err));
  }

  onPageChange($event) {

    this.resultsSubscription = this.searchSearvice.searchPerson(this._query, $event.pageIndex || 1).subscribe(data => {
      this.results = data.results;
      this.totalResults = data.total_results;
    },
    err => console.error('Error: ', err));
  }

}
