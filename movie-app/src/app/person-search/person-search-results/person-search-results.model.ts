export interface PersonSearchResults {
    page?:          number;
    results?:       Result[];
    total_results?: number;
    total_pages?:   number;
}

export interface Result {
    profile_path?: null | string;
    adult?:        boolean;
    id?:           number;
    known_for?:    KnownFor[];
    name?:         string;
    popularity?:   number;
}

export interface KnownFor {
    poster_path?:       string;
    adult?:             boolean;
    overview?:          string;
    release_date?:      Date;
    original_title?:    string;
    genre_ids?:         number[];
    id?:                number;
    media_type:        MediaType;
    original_language?: string;
    title?:             string;
    backdrop_path?:     null | string;
    popularity?:        number;
    vote_count?:        number;
    video?:             boolean;
    vote_average?:      number;
    first_air_date?:    Date;
    origin_country?:    string[];
    name?:              string;
    original_name?:     string;
}

export enum MediaType {
    Movie = "movie",
    Tv = "tv",
}