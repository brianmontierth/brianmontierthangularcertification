import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-person-search-filters',
  templateUrl: './person-search-filters.component.html',
  styleUrls: ['./person-search-filters.component.scss']
})
export class PersonSearchFiltersComponent implements OnInit {

  // properties with getters and setters
  // tslint:disable-next-line: variable-name
  private _query: string;
  public get query(): string {
    return this._query;
  }
  public set query(v: string) {
    this._query = v;
  }

  form = new FormGroup({
    // A required search query.
    query: new FormControl('', [Validators.required])
  });

  // tslint:disable-next-line: variable-name
  constructor(private _router: Router, private _route: ActivatedRoute) { }

  ngOnInit(): void {
    this.handleQueryStrings();
  }

  /** Submit the person search form */
  onSubmit() {

    // if the form is invalid, do nothing.
    if (this.form.invalid) {
      return;
    }

    // update the url with query string.
    // The form data should be reflected in the application URL,
    // for example if a user searched for `Fred`,
    // the URL should be updated to `/search/person?query=fred`.
    this._router.navigate([], {
      relativeTo: this._route,
      queryParams: {
        query: this.form.get('query').value
      },
    })

  }

  /** read query strings and assign properties */
  private handleQueryStrings() {
    this.query = this._route.snapshot.queryParamMap.get('query');

    if (this.query){
      this.form.get('query').setValue(this.query);
    }
  }

}
