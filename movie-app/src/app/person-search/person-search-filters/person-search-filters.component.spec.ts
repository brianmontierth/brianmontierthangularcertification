import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonSearchFiltersComponent } from './person-search-filters.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { PersonSearchModule } from '../person-search.module';

describe('PersonSearchFiltersComponent', () => {
  let component: PersonSearchFiltersComponent;
  let fixture: ComponentFixture<PersonSearchFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule, PersonSearchModule],
      declarations: [ PersonSearchFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonSearchFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
