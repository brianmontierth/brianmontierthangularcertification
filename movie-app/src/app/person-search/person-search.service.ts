import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PersonSearchResults } from './person-search-results/person-search-results.model';

@Injectable({
  providedIn: 'root'
})
export class PersonSearchService {

  // The Movie Database API person search endpoint
  private endpoint = 'https://api.themoviedb.org/3/search/person';

  // api key handled in intercepter.
  // private API_KEY = "e33091b2f060705791b4278f583bd515";


  constructor(private http: HttpClient) { }

  // #Observables
  // Calls the endpoint and returns Observable of Person data
  public searchPerson(query: string, page = 1): Observable<PersonSearchResults> {
    return this.http.get<PersonSearchResults>(this.endpoint + '?page=' + page + '&query=' + query);
  }
}
