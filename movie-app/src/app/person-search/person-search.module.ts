import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonSearchComponent } from './person-search/person-search.component';
import { PersonSearchFiltersComponent } from './person-search-filters/person-search-filters.component';
import { PersonSearchResultsComponent } from './person-search-results/person-search-results.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { AppRoutingModule } from '../app-routing.module';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { PersonSearchService } from './person-search.service';
import { ManageFavoriteButtonModule } from '../manage-favorite-button/manage-favorite-button.module';

// You should be able to route to this search page. (person search page)
const routes: Routes = [
  {
    path: 'search/person',
    component: PersonSearchComponent
  },
];

// #Modules
@NgModule({
  declarations: [
    PersonSearchComponent,
    PersonSearchFiltersComponent,
    PersonSearchResultsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    RouterModule,
    MatPaginatorModule,
    MatTableModule,
    AppRoutingModule,
    MatIconModule,
    MatButtonModule,
    ManageFavoriteButtonModule
  ],
  exports: [
    PersonSearchComponent,
    PersonSearchFiltersComponent,
    PersonSearchResultsComponent
  ]
})
export class PersonSearchModule { }
