import { AuthService } from './auth/auth.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'movie-app';

  constructor(private authService: AuthService, public router: Router) {}

  logout(event: Event) {
    event.stopPropagation();
    event.preventDefault();
    this.authService.logout();
    this.router.navigate(['login'])
  }

  isLoggedIn(): boolean {
    return this.authService.isAuthenticated();
  }
}
