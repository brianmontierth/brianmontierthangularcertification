import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieSearchResultsComponent } from './movie-search-results.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { MovieSearchService } from '../movie-search.service';

describe('MovieSearchResultsComponent', () => {
  let component: MovieSearchResultsComponent;
  let fixture: ComponentFixture<MovieSearchResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
      declarations: [ MovieSearchResultsComponent ],
      providers: [MovieSearchService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
