import { ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { MovieSearchService } from '../movie-search.service';
import { Result } from './movie-search-results.models';
import { MatPaginator } from '@angular/material/paginator';
import { MovieCollectionService } from 'src/app/movie-collection.service';

@Component({
  selector: 'app-movie-search-results',
  templateUrl: './movie-search-results.component.html',
  styleUrls: ['./movie-search-results.component.scss']
})
export class MovieSearchResultsComponent implements OnInit, OnDestroy {

  private _query: string;
  private _year: string;

  public noResults = false;
  public results: Result[];
  public totalResults: number;

  @ViewChild('paginator', {static: false}) paginator: MatPaginator;

  private queryParamsSubscription: Subscription;
  private resultsSubscription: Subscription;

  displayedColumns: string[] = ['Title', 'Release Date', 'Favorite'];

  constructor(private searchSearvice: MovieSearchService,
              private _route: ActivatedRoute,
              public movieCollection: MovieCollectionService) { }

  ngOnInit(): void {
    this.queryParamsSubscription = this._route.queryParams.subscribe(params => this.paramsChanged(params));
  }

  // #Lifecycle hooks
  ngOnDestroy(): void {
    if (this.queryParamsSubscription) {
      this.queryParamsSubscription.unsubscribe();
    }

    if (this.resultsSubscription) {
      this.resultsSubscription.unsubscribe();
    }
  }

  private paramsChanged(params: Params) {
    if (this.paginator) {
      this.paginator.pageIndex = 0;
    }

    this._query = params['query'];
    this._year = params['year'];

    if (!this._query) {
      this.results = [];
      this.totalResults = 0;
      this.noResults = false;
      return;
    }

    // #Destructuring
    this.resultsSubscription = this.searchSearvice.searchMovies(this._query, this._year).subscribe(({results, total_results}) => {
    this.results = results;
    this.totalResults = total_results;

    this.noResults = this.totalResults < 1;
   },
   err => console.error('Error: ', err));
  }

  onPageChange($event) {

    this.resultsSubscription = this.searchSearvice.searchMovies(this._query, this._year, $event.pageIndex || 1).subscribe(data => {
      this.results = data.results;
      this.totalResults = data.total_results;
    },
    err => console.error('Error: ', err));
  }

}
