import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieSearchFiltersComponent } from './movie-search-filters.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MovieSearchModule } from '../movie-search.module';

describe('MovieSearchFiltersComponent', () => {
  let component: MovieSearchFiltersComponent;
  let fixture: ComponentFixture<MovieSearchFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule, BrowserAnimationsModule, MovieSearchModule],
      declarations: [ MovieSearchFiltersComponent ],
      providers: []
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieSearchFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
