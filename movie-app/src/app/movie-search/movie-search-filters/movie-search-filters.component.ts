import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Route, Router} from '@angular/router';

@Component({
  selector: 'app-movie-search-filters',
  templateUrl: './movie-search-filters.component.html',
  styleUrls: ['./movie-search-filters.component.scss']
})
export class MovieSearchFiltersComponent implements OnInit {

  // properties with getters and setters
  private _query: string;
  public get query(): string {
    return this._query;
  }
  public set query(v: string) {
    this._query = v;
  }

  private _year: string;
  public get year(): string {
    return this._year;
  }
  public set year(v: string) {
    this._year = v;
  }

  constructor(private _router: Router, private _route: ActivatedRoute) { }
  // #Forms
  form = new FormGroup({
    // #Validation
    // A required search query.
    query: new FormControl('', [Validators.required]),

    // An optional year.
    year: new FormControl('', [Validators.minLength(4), Validators.maxLength(4)])
  });

  ngOnInit(): void {
    this.handleQueryStrings();
  }

  /** Submit the movie search form */
  onSubmit() {

    // if the form is invalid, do nothing.
    if (this.form.invalid) {
      return;
    }

    // update the url with query strings.
    // The form data should be reflected in the application URL,
    // for example if a user searched for `back` with a year of `1985`,
    // the URL should be updated to `/search/movie?query=back&year=1985`.
    this._router.navigate([], {
      relativeTo: this._route,
      queryParams: {
        query: this.form.get('query').value,
        year: this.form.get('year').value
      },
    });

  }

  /** read query strings and assign properties */
  private handleQueryStrings() {
    this.query = this._route.snapshot.queryParamMap.get('query');
    this.year = this._route.snapshot.queryParamMap.get('year');

    if (this.query){
      this.form.get('query').setValue(this.query);
    }

    if (this.year) {
      this.form.get('year').setValue(this.year);
    }
  }

}
