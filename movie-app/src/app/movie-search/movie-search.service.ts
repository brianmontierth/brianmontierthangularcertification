import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MovieData } from './movie-search-results/movie-search-results.models';

@Injectable({
  providedIn: 'root'
})
/** The search service should perform the following actions: */
/** - Make an HTTP request to [The Movie Database API search endpoint](https://developers.themoviedb.org/3/search/search-movies). */
export class MovieSearchService {

  // The Movie Database API search endpoint
  private endpoint = 'https://api.themoviedb.org/3/search/movie';

  // api key handled in intercepter.
  // private API_KEY = "e33091b2f060705791b4278f583bd515";


  constructor(private http: HttpClient) { }

  // Calls the endpoint and returns Observable of movie data
  public searchMovies(query: string, year: string, page = 1): Observable<MovieData> {
    return this.http.get<MovieData>(this.endpoint + '?page=' + page + '&query=' + query + '&year=' + year);
  }
}
