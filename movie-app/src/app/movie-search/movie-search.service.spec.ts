import { Observable } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { MovieSearchService } from './movie-search.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('SearchService', () => {
  let service: MovieSearchService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      providers: [MovieSearchService]
    });
    service = TestBed.inject(MovieSearchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return results', () => {
    let results = service.searchMovies('back', '1985');
    expect(results).toBeInstanceOf(Observable);
  })
});
