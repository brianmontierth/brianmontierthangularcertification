import { AppRoutingModule } from '../app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieSearchComponent } from './movie-search/movie-search.component';
import { MovieSearchFiltersComponent } from './movie-search-filters/movie-search-filters.component';
import { MovieSearchResultsComponent } from './movie-search-results/movie-search-results.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MovieSearchService } from './movie-search.service';
import { MatIconModule } from '@angular/material/icon';
import { ManageFavoriteButtonModule } from '../manage-favorite-button/manage-favorite-button.module';

// Create a `/search/movie` route in the search module that routes to the movie search component.
const routes: Routes = [
  {
    path: 'search/movie',
    component: MovieSearchComponent,
  }
];

@NgModule({
  declarations: [MovieSearchComponent, MovieSearchFiltersComponent, MovieSearchResultsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    RouterModule,
    MatPaginatorModule,
    MatTableModule,
    AppRoutingModule,
    MatIconModule,
    MatButtonModule,
    ManageFavoriteButtonModule
  ],
  exports: [
    MovieSearchComponent,
    MovieSearchFiltersComponent,
    MovieSearchResultsComponent

  ]
})
export class MovieSearchModule { }
