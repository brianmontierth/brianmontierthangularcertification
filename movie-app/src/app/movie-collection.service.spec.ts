import { TestBed } from '@angular/core/testing';

import { MovieCollectionService } from './movie-collection.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('MovieCollectionService', () => {
  // #Blocked-scoped variables - `let`
  let service: MovieCollectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule]
    });
    service = TestBed.inject(MovieCollectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
