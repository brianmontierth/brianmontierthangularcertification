import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule, Routes } from '@angular/router';
import { ManageFavoriteButtonModule } from '../manage-favorite-button/manage-favorite-button.module';

// Create a `/details/movie/:movieId` route that navigates to the movie details component.
const routes: Routes = [
  {
    path: 'details/movie/:movieId',
    component: MovieDetailsComponent
  },
];

@NgModule({
  declarations: [MovieDetailsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatIconModule,
    MatButtonModule,
    RouterModule,
    ManageFavoriteButtonModule
  ],
  exports: [MovieDetailsComponent],
})
export class MovieDetailsModule { }
