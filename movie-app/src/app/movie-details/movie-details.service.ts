/* eslint-disable no-useless-constructor */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MovieDetails } from './movie-details.model';

@Injectable({
  providedIn: 'root'
})
/*
    The movie details service should perform the following actions:
    - Make an HTTP request to [The Movie Database API movie details endpoint]
    (https://developers.themoviedb.org/3/movies/get-movie-details).
*/
export class MovieDetailsService {
  // The Movie Database API search endpoint
  private endpoint = 'https://api.themoviedb.org/3/movie/';

  // api key handled in intercepter.
  // private API_KEY = "e33091b2f060705791b4278f583bd515";

  constructor(private http: HttpClient) { }

  // Calls the endpoint and returns Observable of movie data
  // #closure
  public getMovieDetails(movieId: string): Observable<MovieDetails> {
    // #this
    return this.http.get<MovieDetails>(this.endpoint + movieId);
  }
}
