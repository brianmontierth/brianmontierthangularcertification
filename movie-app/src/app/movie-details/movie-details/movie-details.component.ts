import { Component, OnInit, OnDestroy } from '@angular/core';
import { MovieDetailsService } from '../movie-details.service';
import { ActivatedRoute } from '@angular/router';
import { MovieDetails } from '../movie-details.model';
import { AuthService } from 'src/app/auth/auth.service';
import { MovieCollectionService } from 'src/app/movie-collection.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {

  // tslint:disable: variable-name
  private _movieId: string;
  public get movieId(): string {
    return this._movieId;
  }
  public set movieId(v: string) {
    this._movieId = v;
  }

  public movieData: MovieDetails;

  constructor(private _detailsService: MovieDetailsService,
              private route: ActivatedRoute,
              private auth: AuthService,
              public collection: MovieCollectionService) {
    // #Arrow functions
    this.route.params.subscribe( params => this.movieId = params && params.movieId as string || '');
  }

  ngOnInit(): void {
    if (this.movieId) {
      this._detailsService.getMovieDetails(this.movieId).subscribe(
        data => this.movieData = data,
        err => console.error('Error: ', err)
      );
    }
  }

  public getBackdropStyle(): string {
    const url = 'url(\'https://image.tmdb.org/t/p/original' + this.movieData.backdrop_path + '\')';
    return url;
  }

  public get isLoggedIn(): boolean {
    return this.auth.isAuthenticated();
  }
}
