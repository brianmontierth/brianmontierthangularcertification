import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonDetailsComponent } from './person-details/person-details.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule, Routes } from '@angular/router';
import { ManageFavoriteButtonComponent } from '../manage-favorite-button/manage-favorite-button.component';
import { ManageFavoriteButtonModule } from '../manage-favorite-button/manage-favorite-button.module';

// Create a `/details/person/:personId` route that navigates to the person details component.
const routes: Routes = [
  {
    path: 'details/person/:personId',
    component: PersonDetailsComponent
  },
];

@NgModule({
  declarations: [PersonDetailsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatIconModule,
    MatButtonModule,
    RouterModule,
    ManageFavoriteButtonModule
  ],
  exports: [PersonDetailsComponent]
})
export class PersonDetailsModule { }
