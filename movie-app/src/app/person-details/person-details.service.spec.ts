import { TestBed } from '@angular/core/testing';

import { PersonDetailsService } from './person-details.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('PersonDetailsService', () => {
  let service: PersonDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule]
    });
    service = TestBed.inject(PersonDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
