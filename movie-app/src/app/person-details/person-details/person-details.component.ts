import { Component, OnInit } from '@angular/core';
import { PersonDetails } from '../person-details.model';
import { PersonDetailsService } from '../person-details.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { PersonCollectionService } from 'src/app/person-collection.service';
import { PersonCredits, Cast, Crew } from '../person-credits.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.scss']
})
export class PersonDetailsComponent implements OnInit {

  // tslint:disable: variable-name
  private _personId: string;
  public get personId(): string {
    return this._personId;
  }
  public set personId(v: string) {
    this._personId = v;
  }

  public personDetails: PersonDetails;
  public personCredits: PersonCredits;
  public castCredits: Cast[];
  public crewCredits: Crew[];

  constructor(private _detailsService: PersonDetailsService,
              private route: ActivatedRoute,
              private auth: AuthService,
              public collection: PersonCollectionService) {
    // #Array Functions - Map
    this.route.params.pipe(map(params => params && params.personId as string)).subscribe(id => this.personId = id);
  }

  ngOnInit(): void {
    if (this.personId) {
      this._detailsService.getPersonDetails(this.personId).subscribe(
        data => this.personDetails = data,
        err => console.error('Error: ', err)
      );

      // #Promise
      this._detailsService.getPersonCredits(this.personId).then(data => {
        // only look at previously released movies
        this.castCredits = data.cast.filter(x => new Date(x.release_date) < new Date() && x.character)
            // sort by released date descending
            .sort((a, b) => {
              return new Date(b.release_date).getTime() - new Date(a.release_date).getTime();
            })
            // only return most recent 5
            .slice(0, 5);
        // only look at previously released movies
        this.crewCredits = data.crew.filter(x => new Date(x.release_date) < new Date())
            // sort by released date descending
            .sort((a, b) => {
              return new Date(b.release_date).getTime() - new Date(a.release_date).getTime();
            })
            // only return most recent 5
            .slice(0, 5);
      })
      .catch(reason => console.error("getPersonCredits failed.", reason));
    }
  }

  public get isLoggedIn(): boolean {
    return this.auth.isAuthenticated();
  }

  getMostRecentCast(credits: PersonCredits): Cast[] {

    // only look at previously released movies
    const cast = credits && credits.cast && credits.cast.filter(x => x.release_date < new Date());

    if (!cast) {
      return null;
    }
    // sort by released date descending
    cast.sort((a, b) => {
      return new Date(b.release_date).getTime() - new Date(a.release_date).getTime();
    });

    // only return most recent 5
    return cast.slice(0, 5);
  }

}
