import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PersonDetails } from './person-details.model';
import { Observable } from 'rxjs';
import { PersonCredits } from './person-credits.model';

@Injectable({
  providedIn: 'root'
})
// https://developers.themoviedb.org/3/people/get-person-details
export class PersonDetailsService {

  // The Movie Database API search endpoint
  private endpoint = 'https://api.themoviedb.org/3/person/';

  // api key handled in intercepter.
  // private API_KEY = "e33091b2f060705791b4278f583bd515";


  constructor(private http: HttpClient) { }

  // Calls the endpoint and returns Observable of person data
  public getPersonDetails(personId: string): Observable<PersonDetails> {
    return this.http.get<PersonDetails>(this.endpoint + personId);
  }

  // Calls the endpoint/movie_credits and returns Observable of person credits
  // #Promise
  public getPersonCredits(personId: string): Promise<PersonCredits> {
    return this.http.get<PersonCredits>(this.endpoint + personId + "/movie_credits").toPromise();
  }
}
