export interface PersonCredits {
    cast?: Cast[];
    crew?: Crew[];
    id?:   number;
}

export interface Cast {
    character?:         string;
    credit_id?:         string;
    poster_path?:       string;
    id?:                number;
    video?:             boolean;
    vote_count?:        number;
    adult?:             boolean;
    backdrop_path?:     null | string;
    genre_ids?:         number[];
    original_language?: string;
    original_title?:    string;
    popularity?:        number;
    title?:             string;
    vote_average?:      number;
    overview?:          string;
    release_date?:      Date;
}

export interface Crew {
    id?:                number;
    department?:        string;
    original_language?: string;
    original_title?:    string;
    job?:               string;
    overview?:          string;
    vote_count?:        number;
    video?:             boolean;
    release_date?:      string;
    vote_average?:      number;
    title?:             string;
    popularity?:        number;
    genre_ids?:         number[];
    backdrop_path?:     null | string;
    adult?:             boolean;
    poster_path?:       null | string;
    credit_id?:         string;
}