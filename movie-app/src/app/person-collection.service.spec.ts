import { TestBed } from '@angular/core/testing';

import { PersonCollectionService } from './person-collection.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('PersonCollectionService', () => {
  let service: PersonCollectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule]
    });
    service = TestBed.inject(PersonCollectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
