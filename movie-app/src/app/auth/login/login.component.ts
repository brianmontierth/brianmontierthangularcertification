import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  error: string;
  loading = false;

  // form variables
  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });
  showPassword = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private authService: AuthService) {

    // The `/login` route should redirect to `/dashboard` if the user is logged in.
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['dashboard']);
  }
   }

  ngOnInit(): void {
  }

  onSubmit() {


    if (this.loginForm.invalid) {
      return;
  }

    this.loading = true;
    this.authService.login(this.loginForm.controls.username.value, this.loginForm.controls.password.value)
  .pipe(first())
  .subscribe(
      data => {
          this.router.navigate(['dashboard']);
      },
      error => {
          this.error = error;
          this.loading = false;
      });
  }

}
