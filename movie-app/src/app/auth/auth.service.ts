import { Injectable } from '@angular/core';
import { of, Observable, BehaviorSubject, config } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { HttpClient, HttpResponse } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
   }

   public get currentUserValue() {
    return this.currentUserSubject.value;
}



login(username: string, password: string) {

  function ok(body?) {
    return of(new HttpResponse({ status: 200, body }));
  }

  return of(new HttpResponse({ status: 200, body: {id: 1, firstName: 'Brian', lastName: 'Montierth', username: 'test', token: 'fake-jwt-token'}}))
        .pipe(map(user => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.currentUserSubject.next(user);
            return user;
        }))
        .pipe(delay(1500));
}

logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
}

  public isAuthenticated(): boolean {
    return this.currentUserSubject.value;
  }
}
