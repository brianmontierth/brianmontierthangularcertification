import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';

@Injectable()
export class CustomHttpInterceptor implements HttpInterceptor {
  constructor() {}

  // Since multiple services will require the API key you should create
  // an HTTP interceptor in the app folder that is responsible for adding the API key to your requests.
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // #Blocked-scoped variables - `const`
    const newParams = req.params.append('api_key', 'e33091b2f060705791b4278f583bd515');
    const clone = req.clone({params: newParams});
    return next.handle(clone);
  }
}
