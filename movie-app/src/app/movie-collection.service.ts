import { Injectable } from '@angular/core';
import { MovieDetails } from './movie-details/movie-details.model';
import { MovieDetailsService } from './movie-details/movie-details.service';

@Injectable({
  providedIn: 'root'
})
export class MovieCollectionService {
  private key = 'movie-collection';
  // tslint:disable-next-line: variable-name
  private _collection: MovieDetails[] = [];

  public get collection(): MovieDetails[] {
    return this._collection;
  }

  constructor(private movieDetailsService: MovieDetailsService) {
    this._collection = JSON.parse(localStorage.getItem(this.key));
  }

  // Checks if the movie is in the collection
  public isInCollection(id: number): boolean {
    if (this._collection && this._collection.find(x => x.id === id)) {
      return true;
    }
    return false;
  }

  // Adds a movie to the collection
  public addMovie(movie: MovieDetails) {
    if (this.isInCollection(movie.id)) {
      return;
    }

    if (!this.collection) {
      this._collection = [movie];
    } else {
      this._collection.push(movie);
    }
    localStorage.setItem(this.key, JSON.stringify(this._collection));
  }

  // Removes a movie from the collection
  public removeMovie(movie: MovieDetails) {
    if (!this.isInCollection(movie.id)) {
      return;
    }
    // #Array Functions - Filter
    this._collection = this._collection.filter(x => x.id !== movie.id);
    localStorage.setItem(this.key, JSON.stringify(this._collection));
  }

  // Adds a movie to the collection by ID
  public addMovieById(id: number) {
    if (this.isInCollection(id)) {
      return;
    }

    this.movieDetailsService.getMovieDetails('' + id).subscribe(resp => {
      if (!resp) {
        return;
      }
      if (this.isInCollection(id)) {
        return;
      }
      if (!this.collection) {
        this._collection = [resp];
      } else {
        this._collection.push(resp);
      }
      localStorage.setItem(this.key, JSON.stringify(this._collection));
    },
    err => console.error('Error: ', err));
  }

  // Removes a movie from the collection by ID
  public removeMovieById(id: number) {
    if (!this.isInCollection(id)) {
      return;
    }
    this._collection = this._collection.filter(x => x.id !== id);
    localStorage.setItem(this.key, JSON.stringify(this._collection));
  }

  // Sort collection by title
  public sortByTitle(): void {
    if (!this._collection) {
      return;
    }

    this._collection.sort((a, b) => {
      // Uppercase so it's case-insensative
      const textA = a.title.toUpperCase();
      const textB = b.title.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
    localStorage.setItem(this.key, JSON.stringify(this._collection));
  }
}
