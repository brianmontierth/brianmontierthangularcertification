import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { MovieSearchComponent } from './movie-search/movie-search/movie-search.component';
import { MovieDetailsComponent } from './movie-details/movie-details/movie-details.component';
import { PersonSearchComponent } from './person-search/person-search/person-search.component';
import { PersonDetailsComponent } from './person-details/person-details/person-details.component';

const routes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
