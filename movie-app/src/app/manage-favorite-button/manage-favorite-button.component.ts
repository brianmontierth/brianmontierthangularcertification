import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../auth/auth.service';

// #Components
@Component({
  selector: 'app-manage-favorite-button',
  templateUrl: './manage-favorite-button.component.html',
  styleUrls: ['./manage-favorite-button.component.scss']
})
export class ManageFavoriteButtonComponent implements OnInit {

  // #Inputs
  @Input() item: any;
  @Input() isFavorite: boolean;

  // #Outputs
  @Output() addItemEvent = new EventEmitter<any>();
  @Output() removeItemEvent = new EventEmitter<any>();



  constructor(private auth: AuthService) { }

  ngOnInit(): void {
  }

  public addItem(value: any) {
    this.addItemEvent.emit(value);
  }

  public removeItem(value: any) {
    this.removeItemEvent.emit(value);
  }

  public get isLoggedIn(): boolean {
    return this.auth.isAuthenticated();
  }

}
