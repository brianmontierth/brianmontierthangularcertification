import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageFavoriteButtonComponent } from './manage-favorite-button.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('ManageFavoriteButtonComponent', () => {
  let component: ManageFavoriteButtonComponent;
  let fixture: ComponentFixture<ManageFavoriteButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
      declarations: [ ManageFavoriteButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageFavoriteButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
