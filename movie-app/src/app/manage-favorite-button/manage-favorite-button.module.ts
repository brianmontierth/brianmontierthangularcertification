import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { ManageFavoriteButtonComponent } from '../manage-favorite-button/manage-favorite-button.component';
import { AuthModule } from '../auth/auth.module';



@NgModule({
  declarations: [ManageFavoriteButtonComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    RouterModule,
    AuthModule
  ],
  exports: [ManageFavoriteButtonComponent]
})
export class ManageFavoriteButtonModule { }